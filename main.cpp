#include <iostream>
#include <vector>


using namespace std;



int SimpleNum(int a) // определение простого числа
{
    for(int i=2;i <= a/2;i++)
        if( (a % i) == 0 )
            return 0;
    return 1;
}

int SumCif(int ChislvMas, int SumOfCif)
{
    while (ChislvMas != 0)
    {
        SumOfCif += ChislvMas % 10;
        ChislvMas = ChislvMas / 10;
    }
    return SumOfCif;
}

int main()
{
    int a;
    cout<<"Введите количество чисел в последовательности: ";
    cin>>a;
    vector<int> v(a);
    cout<<"Введите саму последовательность: ";

    for(int i = 0; i < a; i++)
        cin>>v[i];

    for (int i = 0; i < v.size(); i++)
    {
        if (SimpleNum(v[i]) && ( v[i] != 1) && ( v[i] != 0))
        {
            v.erase(v.begin() + i);
            i--;
        }
    }

    int ChislvMas;
    int SumOfCif = 0;


    for(int i = 0 ; i < v.size() ; i++)// сумма цифр числа
    {
        ChislvMas = v[i];

        if( SumCif(ChislvMas,SumOfCif) == 15)
        {
            v.insert(v.begin() + i + 1, v[i]);
            i++;
        }
        SumOfCif = 0;
    }

    for(int i = 0; i < v.size(); i++)
        cout<<v[i]<<' ';

    return 0;
}